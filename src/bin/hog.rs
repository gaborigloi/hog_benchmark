// TODO help Piston imageproc with benchmarking, comparing libraries? https://github.com/PistonDevelopers/imageproc/issues/3

// TODO build other code with build.rs with crate, https://github.com/ticki/cake ?

extern crate criterion;
extern crate image;
extern crate imageproc;

use criterion::{Bencher, Criterion};

use std::path::Path;
use std::process::Command;
use std::io::Cursor;

use image::imageops::grayscale;

use imageproc::hog::hog;
use imageproc::hog::HogOptions;

fn main() {
    let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                          image::PNG)
                  .unwrap();
    let img_grayscale = grayscale(&img);
    let hog_options = HogOptions::new(18, true, 8, 2, 1);
    let imageproc_hog = |b: &mut Bencher| {
        b.iter(|| hog(&img_grayscale, hog_options));
    };
    let imageproc_hog_grayscale = |b: &mut Bencher| {
        b.iter(|| {
            let img_grayscale = grayscale(&img);
            hog(&img_grayscale, hog_options)
        });
    };
    fn imageproc_hog_grayscale_load_image(b: &mut Bencher) {
        b.iter(|| {
            let img = image::open(&Path::new("resources/pigeons.png")).unwrap();
            let img_grayscale = grayscale(&img);
            let hog_options = HogOptions::new(18, true, 8, 2, 1);
            hog(&img_grayscale, hog_options)
        });
    }
    Criterion::default()
        .sample_size(10)
        .noise_threshold(0.05)
        .bench_function("imageproc_hog", imageproc_hog)
        .bench_function("imageproc_hog_grayscale", imageproc_hog_grayscale)
        .bench_function("imageproc_hog_grayscale_load_image",
                        imageproc_hog_grayscale_load_image)
        .bench_program("opencv3_hog", Command::new("other_src/opencv_3/hog_bench"))
        .bench_program("opencv3_hog_load_image",
                       Command::new("other_src/opencv_3/hog_load_image_bench"));
}
