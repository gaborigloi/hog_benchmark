/**
 * Benchmark hog computation using criterion.rs
 */
// from http://web.cs.sunyit.edu/~realemj/guides/installOpenCV.html
// https://www.quora.com/What-is-the-easiest-way-to-calculate-the-time-elapsed-in-C++

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <string>
#include <chrono>

using namespace cv;
using namespace std;
using ns = chrono::nanoseconds;
using get_time = chrono::high_resolution_clock ;

int main(int argc, char **argv) {
    // For each line in stdin
    int iters;
    // Parse line as the number of iterations
    while (cin >> iters) {
        // Setup

        // Benchmark
        auto start = get_time::now();
        // Execute the routine "iters" times
        for (int i = 0; i < iters; i++) {
            // Code to benchmark goes here
            // Read the image
            UMat imageu = imread("resources/pigeons.png", CV_LOAD_IMAGE_COLOR).getUMat( ACCESS_READ );
            HOGDescriptor hog;
            std::vector<float> descriptors;
            hog.compute(imageu, descriptors, Size(8, 8));
        }
        auto end = get_time::now();

        // Teardown

        // Report elapsed time in nanoseconds to stdout
        auto diff = end - start;
        cout << chrono::duration_cast<ns>(diff).count() << endl;
    }
    return 0;
}
